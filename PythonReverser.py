#Python Text Reverser
#By Jonathan Holzmann
#Mostly practice for functions needed in my main encryptor project.
#Specifically, I can work on the "import from file" idea here.
""" Basic Trial file
Hello World
This is another line
This is the last line
"""
# list of lines(strings) from the file? then go in prev. order.
#list = ["Hello World","This is another line","This is the last line"]
#Output of the text file would be:
""" Inverted Trial File
enil tsal eht si sihT
enil rehtona si sihT
dlroW olleH
"""
#I can technically switch around ebooks with this, if they're in .txt format...
#My own primitive form of DRM. No copy protection though... I'd have to create a new file type. 

def reverser(text):
    textInverse = ""
    for index in range(1, len(text)+1):
        #if text[-index]== "?": textInverse += (that spanish upside-down question mark.)
        #else:
        textInverse += text[-index]
    return textInverse

"""def lineWork:
    with open("in.txt", 'r') as orig, with open("out.txt", 'w') as trans:
        for line in orig:
            add = reverser(line)
            trans.write(add)

need os functions for choosing the files...

"""
def main():
    run = True
    while run == True:
        text = input("What text would you like to reverse? \n")
        if text == "": text = "Hello World"
        elif text.lower() == "stop": break
        print(reverser(text))
        print("If you want to exit the program, type \'STOP\'")

main()

