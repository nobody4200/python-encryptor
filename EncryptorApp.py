#Python redo of cyphering program from AP Comp Sci Prin.
#Created by Jonathan Holzmann
#To provide easy and simple cyphering for casual use.

from graphics import *
import math as m
import random as r


#Documentation
""" There are 3 general uses of this function.
They are presented in order of achievement.
0: Inverter
    Simply reverses the strings (and order in files) of plaintext.
1: cyborg cypher
    This is the most non-intuitive/human. This functionality includes the entirety
    of unicode. It is a Ceasar cypher without limits.
    It is overwhelming, and more secure than the ceasar cypher.
2: ceasar cypher
    This is Ceasar's classic cypher. It is coded like the cyborg cypher,
    but is limited within the range of the alphabet.
    Old and classic, but easily recognizable.
4: One Time Pad
    I'm not sure this is possible.
    It would need a semi-random number generator, with consistant results each time.
    Simulate based on length of the file?
3: pig latin
    This is just in for laughs. However, it is fundamentally different.
    This functionality takes and splits the text based on spacing and then
    applies traditional rules to translate text to pig latin.
"""

""" Format Rules
Cyborg Cypher -> keeps punctuation and spaces, all other characters converted.
Ceasar -> keeps spaces, punctuation, and numerals
Inverter ->does not change any characters
Piglatin: follows all easily implemented 'official' rules, keeps spaces, punctuation and numerals
"""

""" Conversion Algorithms/Concerns:
given: (action: encrypt or decrypt) and "some typed text" or ["Hello World", "something", ... "end of document"]
    for inverter: Already have that mostly figured out.
        Except ideally, I would have it ideally have last line first. But maybe not for this iteration.

    for cyborg: decryption is simple reversal of encryption.
            Add or subtract key from ord(letter), and good to go.
            chr() is inverse of ord, gives unicode from number.
            And also figure out the cases as needed.

    for ceasar: Add in problems with looping round the alphabet.
        modulus the key, and if that goes longer than the remaining letters in alphabet,
        take difference between 26 and that modulus from the modulus, and then resume from the front of the alphabet.
        so key = key % 26, if (oldletterposition + key) > 26, then newletterposition = + key - (26 - key)
        in case of w, w/ key = 10: old letter is 24, so + 10 goes to 34. but instead, 10 -2 = 8, and we looped to h

    For Pig Latin: not getting into THAT yet.
"""


#In-Progress helpmates
def wip(priorityNum):
    priorityNum = str(priorityNum)
    print("Work In Progress. Priority level is: " + priorityNum)
    if priorityNum == '1' or priorityNum.lower() == 'one':
        print("We are working on this feature right now.")

def line():
    print("-----------------------------------------------")
    
#File translation and general first steps


def baseTextFromFile(gunk):
    wip(2)
    '''
    listOfText = []
    for line in file:
        nextLine = file.readline()
        listOfText.append(nextLine)
    return listOfText
'''
    #Difficulty is having person correctly import/chose the correct .txt file. 
    #Needs a lot of help from our old file methods and/or os stuff. Tricky.

def actionDiscovery():
    passMode = False
    while passMode == False:
        action = input("Do you want to encrypt or decrypt some text?\n"+
                       "type \'encrypt\' to encrypt or \'decrypt\' to decrypt.\n")
        if action.lower() == 'stop': break
        elif action.lower() != 'encrypt' and action.lower() != 'decrypt': print("Invalid answer. Type one of the given letters to choose your action")
        else: break
    return action
    
def inputMethodDiscovery(action):
    passMode = False
    inputMethod = None
    while passMode == False:
        modeSwitch = input("Do you want to " + action + " a file or a string?\n" +
                       "type \'string\' to type in a string, and \'file\' to open a .txt file\n")
        if modeSwitch.lower() == 'stop': return None
        elif modeSwitch.lower() == 'string':
            inputMethod = 'text'
            return inputMethod
        elif modeSwitch.lower() == 'file':
            inputMethod = 'file'
            return inputMethod
        else: print("Invalid input. Type one of the given words to choose your text source")
    


#Main call
def interface():
    print("Welcome to the Encryptor App.")
    print("This is a work in progress. Not everything is tested or finalized.")
    print("In Short: Be sure to have copies of important files secure somewhere else.")
#def intent(): Finds intent of user. 4 different states,
    #dealing with binary source and binary action vars 
    line()
    action = actionDiscovery()
    line()
    inputMethod = inputMethodDiscovery(action)
    if inputMethod == 'file':
        fileDirectory = input("What text file do you want to "+ action+"?")
        baseText = baseTextFromFile(fileDirectory) #-> can I pull up the file explorer for that?
    elif inputMethod == 'text':
        baseText = input("Type some text here. Enter will end the message. \n")
        print("Your message is: " + baseText)
    else: print("Critical error. You've found a bug", "Or you just wrote'stop'. exiting now")
    #Just FYI: will prob. get weird errors because of "stop" command in other functions.
    #Figure out the except stuff again. Not in onenote?
    wip(1)
#def conversion(): the actual functionality of the program.
#This is where it gets 'fun'.

#Pig Latin: work on later
def left2(str):
    return str[2:] + str[:2]
    print("I am focusing on this branch and added a good set of features")
    print('progress was made')


interface()
