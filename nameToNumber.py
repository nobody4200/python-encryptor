#Lucky Number Generator:
#By Jonathan Holzmann
#Little function modified off of js encryptor to demonstrate ord() method
# disregards all punctuation, by lowering and using that range.
'''Ideas for later project:
*Multiple exit numbers:
    mult and divide by age,
    modify using even/odd with gender and modulus,
    add a random or two, to spice it up.
*incorporate with the Magic Eight ball code from List Lab Problem Set
    (For their questions)
*Fortune Cookie Simulator
    (if they just want answers)
    Would need to find large sample set, tho.
*Complex user interface structure, as always.
    (Like the old text RPGs)
'''

def basicNameNum():
    string = input("Put in any name that you respond to or are known by.\n" +
                   "Or even your real name.\n" +
                 "Whatever is most comfortable to YOU. ")
    string = string.lower()
    endNumber = 0
    #listofnums = [] Leave all this until later
    for i in string:
        numThatIsChar = ord(i) - 96
        if numThatIsChar >=1 and numThatIsChar <= 26:
            endNumber = endNumber + numThatIsChar
    print("Your number of fate is: " + str(endNumber))
#This little thing shows how to get numerals out of alphanumerical symbols.

basicNameNum()

